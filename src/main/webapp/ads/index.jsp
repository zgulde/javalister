<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>View all ads</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<jsp:include page="/partials/navbar.jsp" />
<div class="container">
    <h1>
        Here are all the ads!
        <a href="/ads/create" class="pull-right btn btn-primary">
            Create A New Ad
        </a>
    </h1>

    <c:if test="${ads.isEmpty()}">
        <h2>No ads here yet...</h2>
    </c:if>

    <c:forEach var="ad" items="${ads}">
        <div class="col-md-6">
            <h2>
                <a href="/ads/show?id=${ad.id}">${ad.title}</a>
            </h2>
        </div>
    </c:forEach>

</div>

</body>
</html>