<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<jsp:include page="/partials/head.jsp">
    <jsp:param name="title" value="Edit your ad" />
</jsp:include>
<body>
<jsp:include page="/partials/navbar.jsp" />
<div class="container">
    <h1>Edit your Ad!</h1>
    <jsp:include page="/partials/messages.jsp" />
    <jsp:include page="/ads/partials/form.jsp">
        <jsp:param name="action" value="/ads/edit" />
    </jsp:include>
</div>
</body>
</html>
