<form action="${param.action}" method="post">

    <input type="hidden" name="id" value="${ad.id}">

    <div class="form-group">
        <label for="title">Title</label>
        <input id="title" name="title" class="form-control" type="text" value="${ad.title}">
    </div>
    <div class="form-group">
        <label for="description">Description</label>
        <input id="description" name="description" class="form-control" type="text" value="${ad.description}">
    </div>

    <input class="btn btn-primary btn-block" type="submit">
</form>
