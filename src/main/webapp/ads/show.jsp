<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<jsp:include page="/partials/head.jsp">
    <jsp:param name="title" value="Viewing ad ${ad.id}" />
</jsp:include>
<body>
    <jsp:include page="/partials/navbar.jsp" />
    <div class="container">
        <h1>${ad.title}</h1>
        <p>${ad.description}</p>
        <p>Posted By ${user.name} - ${user.email}</p>
    </div>
</body>
</html>
