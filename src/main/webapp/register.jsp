<!DOCTYPE html>
<html lang="en">
<jsp:include page="/partials/head.jsp">
    <jsp:param name="title" value="Sign Up" />
</jsp:include>
<body>
    <jsp:include page="/partials/navbar.jsp" />
    <div class="container">
        <h1>Register for our site!</h1>

        <jsp:include page="/partials/messages.jsp" />

        <form action="/register" method="post">
            <div class="form-group">
                <label for="name">Name</label>
                <input id="name" class="form-control" name="name" type="text" value="${user.name}">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input class="form-control" id="email" name="email" type="text" value="${user.email}">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input class="form-control" id="password" name="password" type="password">
            </div>
            <div class="form-group">
                <label for="confirm_password">Confirm Password</label>
                <input class="form-control" id="confirm_password" name="confirm_password" type="password">
            </div>
            <input id="submit" type="submit" class="btn btn-block btn-primary">
        </form>

    </div>

</body>
</html>