<!DOCTYPE html>
<html lang="en">
<jsp:include page="/partials/head.jsp">
    <jsp:param name="title" value="Log in" />
</jsp:include>
<body>
<jsp:include page="/partials/navbar.jsp" />
<div class="container">
    <h1>Please Log In</h1>

    <jsp:include page="/partials/messages.jsp" />

    <form action="/login" method="POST">

        <div class="form-group">
            <label for="">name</label>
            <input type="text" class="form-control" name="name">
        </div>
        <div class="form-group">
            <label for="">password</label>
            <input type="text" class="form-control" name="password">
        </div>

        <input type="submit" class="btn btn-block btn-primary">

    </form>
</div>
</body>
</html>