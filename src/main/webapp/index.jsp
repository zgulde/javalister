<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<jsp:include page="/partials/head.jsp">
    <jsp:param name="title" value="Welcome!" />
</jsp:include>
<body>
    <jsp:include page="/partials/navbar.jsp" />
    <div class="container">
        <h1>Welcome to the AdLister!</h1>
    </div>
</body>
</html>
