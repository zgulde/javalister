<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
    <jsp:include page="/partials/navbar.jsp" />
    <div class="container">
        <h1>Your Profile</h1>
        <p>Name: ${user.name}</p>
        <p>Email: ${user.email}</p>
        <h2>Your Ads</h2>
        <c:forEach var="ad" items="${ads}">
            <div class="col-md-6">
                <h3>
                    <a href="/ads/show?id=${ad.id}">${ad.title}</a>
                </h3>
                <p>${ad.description}</p>
            </div>
        </c:forEach>
    </div>
</body>
</html>
