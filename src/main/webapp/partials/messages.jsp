<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${! sessionScope.errors.isEmpty()}">
    <c:forEach var="error" items="${sessionScope.errors}">
        <p class="alert alert-danger">${error}</p>
    </c:forEach>

    <c:remove var="errors" scope="session" />
</c:if>
<c:if test="${sessionScope.message != null}">
    <p class="alert alert-success">${sessionScope.message}</p>
    <c:remove var="message" scope="session" />
</c:if>