package db;

import java.sql.SQLException;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Created by zach on 11/29/16.
 */
public class AdsSeeder {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        List<Long> ids = UsersDao.all().stream().map(user -> user.getId()).collect(Collectors.toList());
        AdsDao.insert(sample(ids), "some title", "some description");
        AdsDao.insert(sample(ids), "some title 2", "some description 2");
        AdsDao.insert(sample(ids), "some title 3", "some description 3");
        AdsDao.insert(sample(ids), "some title 4", "some description 4");
    }

    public static Long sample(List<Long> ids) {
        return ids.get(new Random().nextInt(ids.size()));
    }
}
