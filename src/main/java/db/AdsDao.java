package db;

import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.math.BigInteger;
import java.sql.SQLException;
import java.util.List;

public class AdsDao extends BaseDao {
    // We need to be specific about which properties we'll select because we need to alias user_id to
    // the camel cased version. This is so apache commons' dbutils can turn the results of a query into
    // an Ad bean
    private static final String PROPS = "id, user_id as userId, title, description";
    private static final String ALL = "SELECT " + PROPS + " FROM ads";
    private static final String INSERT = "INSERT INTO ads (user_id, title, description) VALUES (?, ?, ?)";
    private static final String FIRST = "SELECT " + PROPS + " FROM ads LIMIT 1";
    private static final String FIND = "SELECT " + PROPS + " FROM ads WHERE id = ? LIMIT 1";
    private static final String BY_USER_ID = "SELECT " + PROPS + " FROM ads WHERE user_id = ?";
    private static final String UPDATE = "UPDATE ads SET title = ?, description = ? WHERE id = ?";

    private static ResultSetHandler<List<Ad>> getHandler() {
        return new BeanListHandler<>(Ad.class);
    }

    public static List<Ad> all() throws SQLException, ClassNotFoundException {
        return getQueryRunner().query(getConnection(), ALL, getHandler());
    }

    public static List<Ad> byUserId(long userId) throws SQLException, ClassNotFoundException {
        return getQueryRunner().query(getConnection(), BY_USER_ID, getHandler(), userId);
    }

    public static Ad find(long id) throws SQLException, ClassNotFoundException {
        List<Ad> results = getQueryRunner().query(getConnection(), FIND, getHandler(), id);
        return results.isEmpty() ? null : results.get(0);
    }

    public static Ad first() throws SQLException, ClassNotFoundException {
        List<Ad> results = getQueryRunner().query(getConnection(), FIRST, getHandler());
        return results.isEmpty() ? null : results.get(0);
    }

    public static Long insert(long userId, String title, String description) throws SQLException, ClassNotFoundException {
        return getQueryRunner().insert(
                getConnection(),
                INSERT,
                new ScalarHandler<BigInteger>(),
                userId, title, description
        ).longValue();
    }

    public static int update(long id, String title, String description) throws SQLException, ClassNotFoundException {
        return getQueryRunner().update(
            getConnection(),
            UPDATE,
            title, description, id
        );
    }
}

