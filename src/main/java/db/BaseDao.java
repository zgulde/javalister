package db;

import org.apache.commons.dbutils.QueryRunner;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by zach on 11/29/16.
 */
public class BaseDao {
    public static Connection connection;
    public static QueryRunner queryRunner;

    protected static Connection getConnection() throws SQLException, ClassNotFoundException {
        if (connection == null) {
            Class.forName("com.mysql.cj.jdbc.Driver"); // we only need this with tomcat
            connection = DriverManager.getConnection(
                "jdbc:mysql://localhost/adlister_db",
                "root",
                "codeup"
            );
        }
        return connection;
    }

    protected static QueryRunner getQueryRunner() {
        if (queryRunner == null) {
            queryRunner = new QueryRunner();
        }
        return queryRunner;
    }
}
