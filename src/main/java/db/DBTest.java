package db;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by zach on 11/29/16.
 */
public class DBTest {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        for(User user : UsersDao.all()) {
            System.out.println("id: " + user.getId());
            System.out.println("name: " + user.getName());
            System.out.println("email: " + user.getEmail());
            System.out.println("password: " + user.getPassword());
        }
    }
}
