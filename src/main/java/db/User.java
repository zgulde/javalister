package db;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by zach on 11/29/16.
 */
public class User {
    private long id;
    private String name;
    private String email;
    private String password;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static User createFromRequest(HttpServletRequest req) {
        User user = new User();
        user.setName(req.getParameter("name"));
        user.setEmail(req.getParameter("email"));
        return user;
    }

    public boolean canEdit(Ad ad) {
        return this.id == ad.getUserId();
    }
}
