package db;

import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import util.Password;

import java.math.BigInteger;
import java.sql.SQLException;
import java.util.List;

public class UsersDao extends BaseDao {
    private static final String ALL = "SELECT * FROM users";
    private static final String INSERT = "INSERT INTO users (name, email, password) VALUES (?, ?, ?)";
    private static final String FIRST = "SELECT * FROM users LIMIT 1";
    private static final String FIND = "SELECT * FROM users WHERE id = ? LIMIT 1";
    private static final String FIND_BY_NAME = "SELECT * FROM users WHERE name = ? LIMIT 1";

    private static ResultSetHandler<List<User>> getHandler() {
        // Use the BeanListHandler implementation to convert all ResultSet rows
        // into a List of Users JavaBeans.
        return new BeanListHandler<>(User.class);
    }

    public static List<User> all() throws SQLException, ClassNotFoundException {
        return getQueryRunner().query(getConnection(), ALL, getHandler());
    }

    public static User findByName(String name) throws SQLException, ClassNotFoundException {
        List<User> results = getQueryRunner().query(getConnection(), FIND_BY_NAME, getHandler(), name);
        return results.isEmpty() ? null : results.get(0);
    }

    public static User find(long id) throws SQLException, ClassNotFoundException {
        List<User> results = getQueryRunner().query(getConnection(), FIND, getHandler(), id);
        return results.isEmpty() ? null : results.get(0);
    }

    public static User first() throws SQLException, ClassNotFoundException {
        List<User> results = getQueryRunner().query(getConnection(), FIRST, getHandler());
        return results.isEmpty() ? null : results.get(0);
    }

    public static Long insert(String name, String email, String password) throws SQLException, ClassNotFoundException {
        return getQueryRunner().insert(
            getConnection(),
            INSERT,
            new ScalarHandler<BigInteger>(),
            name, email, Password.hash(password)
        ).longValue();
    }
}
