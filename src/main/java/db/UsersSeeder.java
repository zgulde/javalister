package db;

import util.Password;

import java.sql.SQLException;

public class UsersSeeder {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        UsersDao.insert("zach", "zach@codeup.com", "codeup");
        UsersDao.insert("ryan", "ryan@codeup.com", "codeup");
        UsersDao.insert("luis", "luis@codeup.com", "codeup");
        UsersDao.insert("fernando", "fernando@codeup.com", "codeup");
    }
}
