package db;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by zach on 11/29/16.
 */
public class Ad {
    private long id;
    private long userId;
    private String title;
    private String description;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static Ad createFromRequest(HttpServletRequest req) {
        Ad ad = new Ad();
        ad.setDescription(req.getParameter("description"));
        ad.setTitle(req.getParameter("title"));
        return ad;
    }
}
