package servlets;

import db.Ad;
import db.AdsDao;
import db.User;
import db.UsersDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "ProfileServlet", urlPatterns = "/profile")
public class ProfileServlet extends HttpServlet {
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
        if (req.getSession().getAttribute("id") == null) {
            res.sendRedirect("/login");
            return;
        }

        User user = null;
        List<Ad> ads = null;
        try {
            user = UsersDao.find((long) req.getSession().getAttribute("id"));
            ads = AdsDao.byUserId((long) req.getSession().getAttribute("id"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        req.setAttribute("user", user);
        req.setAttribute("ads", ads);

        req.getRequestDispatcher("/profile.jsp").forward(req, res);
    }
}
