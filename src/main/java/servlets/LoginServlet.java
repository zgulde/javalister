package servlets;

import db.User;
import db.UsersDao;
import util.Password;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "LoginServlet", urlPatterns = "/login")
public class LoginServlet extends HttpServlet {
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        req.getRequestDispatcher("/login.jsp").forward(req, res);
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
        User user = null;
        List<String> errors = new ArrayList<>();

        try {
            user = UsersDao.findByName(req.getParameter("name"));
        } catch (Exception e) {
            errors.add("Unable to connect to the database!");
            e.printStackTrace();
        }

        if (user == null) {
            errors.add("No user found with that name.");
        }

        if (! Password.check(req.getParameter("password"), user.getPassword())) {
            errors.add("Invalid password.");
        }

        if (! errors.isEmpty()) {
            req.getSession().setAttribute("errors", errors);
            res.sendRedirect("/login");
            return;
        }

        // check for intended url
        String intended = (String) req.getSession().getAttribute("intended");
        req.getSession().removeAttribute("intended");

        req.getSession().setAttribute("id", user.getId());
        res.sendRedirect(intended != null ? intended : "/profile");
    }
}
