package servlets.ads;

import db.AdsDao;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;

@WebServlet(name = "AdsIndexServlet", urlPatterns = "/ads")
public class AdsIndexServlet extends HttpServlet {
    protected void doGet(HttpServletRequest req, HttpServletResponse res) {
        System.out.println("ads index");
        try {
            req.setAttribute("ads", AdsDao.all());
            req.getRequestDispatcher("/ads/index.jsp").forward(req, res);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
