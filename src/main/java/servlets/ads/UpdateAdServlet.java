package servlets.ads;

import db.Ad;
import db.AdsDao;
import db.User;
import db.UsersDao;
import util.AdValidator;
import util.Validate;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "UpdateAdServlet", urlPatterns = "/ads/edit")
public class UpdateAdServlet extends HttpServlet {
    private Ad ad;

    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
        validateAdExistsAndUserCanEdit(req, res);
        req.setAttribute("ad", ad);
        req.getRequestDispatcher("/ads/edit.jsp").forward(req, res);
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
        validateAdExistsAndUserCanEdit(req, res);
        AdValidator validator = Validate.ad(req.getParameterMap());

        if (validator.fails()) {
            req.getSession().setAttribute("errors", validator.getErrors());
            req.setAttribute("ad", Ad.createFromRequest(req));
            req.getRequestDispatcher("/ads/edit.jsp").forward(req, res);
            return;
        }

        try {
            AdsDao.update(
                ad.getId(),
                req.getParameter("title"),
                req.getParameter("description")
            );
            req.getSession().setAttribute("message", "Ad successfully updated.");
            res.sendRedirect("/ads/show?id=" + ad.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void validateAdExistsAndUserCanEdit(HttpServletRequest req, HttpServletResponse res) throws IOException {
        if (req.getSession().getAttribute("id") == null) {
            // must be logged in to create an ad
            req.getSession().setAttribute("intended", "/ads/create");
            res.sendRedirect("/login");
            return;
        }

        ad = null;
        User user = null;

        try {
            // watch out for number format exception
            ad = AdsDao.find(Long.parseLong(req.getParameter("id")));
            user = UsersDao.find((Long) req.getSession().getAttribute("id"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (! user.canEdit(ad)) {
            res.sendRedirect("/profile");
        }
    }
}
