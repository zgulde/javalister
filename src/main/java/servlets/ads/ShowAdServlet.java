package servlets.ads;

import com.mysql.cj.core.util.StringUtils;
import db.Ad;
import db.AdsDao;
import db.User;
import db.UsersDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "ShowAdServlet", urlPatterns = "/ads/show")
public class ShowAdServlet extends HttpServlet {
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
        Ad ad = null;
        User user = null;
        try {
            // watch out for number format exception
            ad = AdsDao.find(Long.parseLong(req.getParameter("id")));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (ad == null) {
            // redirect back to index page
            res.sendRedirect("/ads");
            return;
        }
        try {
            user = UsersDao.find(ad.getUserId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        req.setAttribute("ad", ad);
        req.setAttribute("user", user);
        req.getRequestDispatcher("/ads/show.jsp").forward(req, res);

    }
}
