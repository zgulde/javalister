package servlets.ads;

import db.Ad;
import db.AdsDao;
import util.AdValidator;
import util.Validate;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "CreateAdServlet", urlPatterns = "/ads/create")
public class CreateAdServlet extends HttpServlet {
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        if (req.getSession().getAttribute("id") == null) {
            // must be logged in to create an ad
            req.getSession().setAttribute("intended", "/ads/create");
            res.sendRedirect("/login");
            return;
        }
        req.setAttribute("ad", new Ad());
        req.getRequestDispatcher("/ads/create.jsp").forward(req, res);
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
        if (req.getSession().getAttribute("id") == null) {
            // must be logged in to create an ad
            res.sendRedirect("/login");
            return;
        }

        AdValidator validator = Validate.ad(req.getParameterMap());

        if (validator.fails()) {
            req.getSession().setAttribute("errors", validator.getErrors());
            req.setAttribute("ad", Ad.createFromRequest(req));
            req.getRequestDispatcher("/ads/create.jsp").forward(req, res);
            return;
        }

        try {
            Long id = AdsDao.insert(
                (Long) req.getSession().getAttribute("id"),
                req.getParameter("title"),
                req.getParameter("description")
            );
            req.getSession().setAttribute("message", "Ad successfully created!");
            res.sendRedirect("/ads/show?id=" + id);
        } catch (Exception e) {
            e.printStackTrace();
            req.getSession().setAttribute("errors", new String[]{"Error talking to the database."});
            res.sendRedirect("/ads/create");
        }
    }
}
