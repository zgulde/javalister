package servlets;

import db.Ad;
import db.User;
import db.UsersDao;
import util.UserValidator;
import util.Validate;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

@WebServlet(name = "RegisterServlet", urlPatterns = "/register")
public class RegisterServlet extends HttpServlet {
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        req.setAttribute("user", new User());
        req.getRequestDispatcher("/register.jsp").forward(req, res);
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
        UserValidator validator = Validate.user(req.getParameterMap());
        if (validator.fails()) {
            req.getSession().setAttribute("errors", validator.getErrors());
            res.setStatus(422);
            req.setAttribute("user", User.createFromRequest(req));
            req.getRequestDispatcher("/register.jsp").forward(req, res);
            return;
        }

        try {
            UsersDao.insert(
                req.getParameter("name"),
                req.getParameter("email"),
                req.getParameter("password")
            );
            req.getSession().setAttribute("message", "Successfully registered! Please log in.");
            res.sendRedirect("/login");
        } catch (Exception e) {
            e.printStackTrace();
            req.getSession().setAttribute(
                "errors",
                new String[]{"Error talking to the database"}
            );
        }
    }
}
