package util;

import java.util.Map;

public class AdValidator extends Validator {
    public AdValidator(Map<String, String[]> attributes) {
        super(attributes);

        validate(
            "title",
            "Please enter a title for the ad.",
            "Title must be at least 5 characters.",
            title -> title.length() >= 5
        ).validate(
            "description",
            "Please enter a description for the ad.",
            "Description must be at least 6 characters.",
            description -> description.length() >= 6
        );
    }
}
