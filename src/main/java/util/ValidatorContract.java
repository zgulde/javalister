package util;

import java.util.List;

public interface ValidatorContract {
    boolean fails();
    boolean passes();
    List<String> getErrors();
}
