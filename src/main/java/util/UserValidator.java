package util;

import org.apache.commons.lang3.StringUtils;

import java.util.Map;

public class UserValidator extends Validator {
    public UserValidator(Map<String, String[]> attributes) {
        super(attributes);

        validate(
            "name",
            "Please enter your name.",
            "Name must be at least 4 characters.",
            name -> name.length() >= 4
        ).validate(
            "email",
            "Please enter your email",
            "Please enter a valid email.",
            email -> StringUtils.contains(email, "@") // this is easier than doing something more complicated
        ).validate(
            "password",
            "Please choose a password.",
            "Password must be at least 5 characters",
            password -> password.length() >= 5
        ).validate(
            "confirm_password",
            "Please confirm your password",
            "Passwords do not match",
            confirmPassword -> confirmPassword.equals(attributes.get("password")[0])
        );
    }

}
