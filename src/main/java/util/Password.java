package util;

import org.mindrot.jbcrypt.BCrypt;

/**
 * Created by zach on 11/29/16.
 */
public class Password {
    public static String hash(String pw) {
        return BCrypt.hashpw(pw, BCrypt.gensalt());
    }

    public static boolean check(String pw, String hash) {
        return BCrypt.checkpw(pw, hash);
    }
}
