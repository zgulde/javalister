package util;

import java.util.Map;

public class Validate {
    public static UserValidator user(Map<String, String[]> attributes) {
        return new UserValidator(attributes);
    }

    public static AdValidator ad(Map<String, String[]> attributes) {
        return new AdValidator(attributes);
    }
}
