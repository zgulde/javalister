package util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

public abstract class Validator implements ValidatorContract {
    protected List<String> errors;
    protected Map<String, String[]> attributes;

    public Validator(Map<String, String[]> attributes) {
        this.attributes = attributes;
        this.errors = new ArrayList<>();
    }

    protected Validator validate(String prop, String missingMessage, String failureMessage, Predicate<String> p) {
        if ((! attributes.containsKey(prop)) || (attributes.get(prop)[0].length() == 0)) {
            errors.add(missingMessage);
        } else if (! p.test(attributes.get(prop)[0])) {
            errors.add(failureMessage);
        }
        return this;
    }

    protected Validator validate(String prop, String message, Predicate<String> p) {
        return validate(prop, "Missing " + prop + ".", message, p);
    }

    @Override
    public boolean fails() {
        return ! errors.isEmpty();
    }

    @Override
    public boolean passes() {
        return errors.isEmpty();
    }

    @Override
    public List<String> getErrors() {
        return errors;
    }
}
