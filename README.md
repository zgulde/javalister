# "Simple" Adlister

This repo contains a two-table adlister. It is far from commplete but all of the
base functionality is there.

## Setup

Import this project as a maven project, then let Intellij and maven do their
things are get all the dependencies.

### Database

Make sure you have a `adlister_db`. Run the migration script in the root of the
project, for example

```
mysql -u root -p < migration.sql
```

Currently the database credentials are hardcoded in the `getConnection` method
in the `db.BaseDao` class. Either setup your database credentials to match mine,
or go change the `BaseDao` class.

There are two seeders that are implemented as java classes with a main method in
the `db` package.

### Running

Setup a run configuration with tomcat.

Run -> Run Configurations
Add New Configuration -> Tomcat -> Local

From the new run configuration click the configure button in the "Application
Server" area.

Point "Tomcat Home" to your local installation of tomcat. If you are on OSX and
installed tomcat with brew, this should be

```
# your version number might be different
/usr/local/Cellar/tomcat/8.5.6/libexec
```

Otherwise if you are on Linux or installed tomcat differently,

```
which catalina
```

Should point you to the installation location

## Notes

I'm using the apache commons db utils for running queries with prepared
statements.

All servlets are configured with the `WebServlet` annotation, as opposed to a
`web.xml`.

## Examples

Validation (kind of), error messages in session flash data, ensuring that a user
is logged in and redirecting if not, partials

- `webapp/ads/create.jsp`
- `webapp/partials/errors.jsp`
- `main/servlets/ads/CreateAdServlet.java`

Logging in a user, verifying passwords, storing logged in user id in the session

- `main/servlets/LoginServlet.java`
- `main/util/Password.java`

Hashing a password when a user is created

- `main/servlets/RegisterServlet.java`
- `main/util/Password.java`

Passing data from a servlet to a jsp

- `webapp/profile.jsp`
- `main/servlets/ProfileServlet.java`

    See the last few lines of the `doGet` method

Iterating through a List to create html, jstl `if` and `forEach`

- `webapp/ads/index.jsp`
- `main/servlets/ads/AdsIndexServlet.java`

Using `GET` request parameters

- `main/servlets/ads/ShowAdServlet.java`
- `webapp/ads/show.jsp`

Creating a database connection

- `main/db/BaseDao.java`

Querying the database

- `main/db/UsersDao.java`
- `main/db/AdsDao.java`

Model classes (they're just beans)

- `main/db/Ad.java`
- `main/db/User.java`
